# Des Admonitions pour les Mathématiques

## Maths Admonitions

### Def / Definition / Définition

=== "Rendering"
    !!! def
        a bit is `0` or `1`
=== ":fa-markdown: def"
    ```bash linenums="0"
    !!! def
        a bit is `0` or `1`
    ```
=== ":fa-markdown: definition"
    ```bash linenums="0"
    !!! definition
        a bit is `0` or `1`
    ```
=== ":fa-markdown: définition"
    ```bash linenums="0"
    !!! définition
        a bit is `0` or `1`
    ```

### Lem / Lemma / Lemme

=== "Rendering"
    !!! lem
        $e^{i\pi}=-1$
=== ":fa-markdown: lem"
    ```bash linenums="0"
    !!! lem
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: lemma"
    ```bash linenums="0"
    !!! lemma
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: lemme"
    ```bash linenums="0"
    !!! lemme
        $e^{i\pi}=-1$
    ```

### Pte / Property / Propriete / Propriété

=== "Rendering"
    !!! pte
        $e^{i\pi}=-1$
=== ":fa-markdown: pte"
    ```bash linenums="0"
    !!! pte
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: property"
    ```bash linenums="0"
    !!! property
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: propriete"
    ```bash linenums="0"
    !!! propriete
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: propriété"
    ```bash linenums="0"
    !!! propriété
        $e^{i\pi}=-1$
    ```

### Prop / Proposition / Proposition

=== "Rendering"
    !!! prop
        $e^{i\pi}=-1$
=== ":fa-markdown: prop"
    ```bash linenums="0"
    !!! prop
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: proposition"
    ```bash linenums="0"
    !!! proposition
        $e^{i\pi}=-1$
    ```

### Thm / Theorem / Théorème

=== "Rendering"
    !!! thm
        $e^{i\pi}=-1$
=== ":fa-markdown: thm"
    ```bash linenums="0"
    !!! thm
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: theorem"
    ```bash linenums="0"
    !!! theorem
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: théorème"
    ```bash linenums="0"
    !!! théorème
        $e^{i\pi}=-1$
    ```

### Ex / Exercice / Exercice

=== "Rendering"
    !!! ex
        1. What's the weather today?
        1. What will the weather be tomorrow?
=== ":fa-markdown: ex"
    ```bash linenums="0"
    !!! ex
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: exo"
    ```bash linenums="0"
    !!! exo
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: exercice"
    ```bash linenums="0"
    !!! exercice
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

### Exp / Examples / Exemples

=== "Rendering"
    !!! exp
        This is an example
=== ":fa-markdown: exp"
    ```bash linenums="0"
    !!! exp
        This is an example
    ```
=== ":fa-markdown: example"
    ```bash linenums="0"
    !!! example
        This is an example
    ```
=== ":fa-markdown: exemple"
    ```bash linenums="0"
    !!! exemple
        This is an example
    ```

### Mth / Method / Méthode

=== "Rendering"
    !!! mth
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: mth"
    ```bash linenums="0"
    !!! mth
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: method"
    ```bash linenums="0"
    !!! method
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: méthode"
    ```bash linenums="0"
    !!! méthode
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

### Python

!!! python
    ```python
    hey = input("your name? ")
    print(hey)
    ```

## Per-Name Auto-numbering

Properties are auto-numbered, as well as theorems, etc.. independently

!!! pte
    Other Property

!!! pte
    Other Property

!!! thm
    Other Theorem

!!! thm
    Other Theorem

!!! thm
    Other Theorem

!!! lem
    a Lemma

## Compatibility with Admonitions

Maths Admonitions are compatible with other Classical Admonitions from Material Theme.

!!! propriété
    === "Énoncé"
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
        nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
        massa, nec semper lorem quam in massa.
    === "Démonstration"
        Blabla $\sqrt 2$