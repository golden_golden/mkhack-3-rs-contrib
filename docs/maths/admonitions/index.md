# Maths Admonitions for mkdocs

## What are Maths Admonitions for mkdocs?

Maths admonitions for mkdocs take advantage of admonitions of the material theme for mkdocs, to define maths usual blocks:

* Property / Propriété (:fr:)
* Theorem / Théorème (:fr:)
* Lemma / Lemme (:fr:)
* Example / Exemple (:fr:)
* Exercice / Exercice (:fr:)
* Method / Méthode (:fr:)
* Python / Python (:fr:)

**Maths Admonitions for mkdocs are compatible with**:

* Admonitions themselves
* Markdown tables
* Graphviz and Mermaid Diagrams

## Configuration

In the `mkdocs.yml` config file, add the following:

* In `extra_css`, add a link to rod2ik's cdn `columns.css`

```yaml linenums="0"
extra_css:
   # For mkdocs integration: Add a link to 'massilia-maths.css' cdn by rod2ik
   - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia-maths.css
extra_javascript:
   # For mkdocs integration: Add a link to 'massilia-maths.js' cdn by rod2ik
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia-maths.js
```

That's all folks !

