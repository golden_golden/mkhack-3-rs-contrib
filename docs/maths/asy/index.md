# The `mkdocs-asy` Extension

<center>

```dot
digraph "example-graphviz" {
     rankdir="LR";
     graph [fontname="Verdana", fontsize="12"];
     node [fontname="Verdana", fontsize="12"];
     edge [fontname="Sans", fontsize="9"];

     sphinx [label="MkDocs", shape="component",
               URL="https://www.mkdocs.org/",
               target="_blank"];
     dot [label="Asymptote", shape="component",
          URL="https://asymptote.sourceforge.io/",
          target="_blank"];
     docs [label="Docs (.md)", shape="folder",
          fillcolor=green, style=filled];
     svg_file [label="SVG Image", shape="note", fontcolor=white,
               fillcolor="#3333ff", style=filled];
     html_files [label="HTML Files", shape="folder",
          fillcolor=yellow, style=filled];

     docs -> sphinx [label=" parse "];
     sphinx -> dot [label=" call ", style=dashed, arrowhead=none];
     dot -> svg_file [label=" draw "];
     sphinx -> html_files [label=" render "];
     svg_file -> html_files [style=dashed];
}
```

</center>

## Installation

### Asymptote package must be installed first

!!! info "Asymptote Package must be installed first"
    Please note that you **must** have the Asymptote binaries installed in your machine at first.  
    e.g. on Manjaro Linux:

    ```bash linenums="0"
    $ sudo pacman -S asymptote
    $ sudo pacman -S python-pyqt5
    $ sudo pacman -S python-cson
    ```

### Installation of `mkdocs-asy`

Install the [`mkdocs-asy`](https://pypi.org/project/mkdocs-asy/) extension via `pip`:

* Installation : `$ pip install mkdocs-asy`

or

* Upgrade : `$ pip install --upgrade mkdocs-asy`

## Configuration

### Activation in `mkdocs.yml`

Activate the `mkdocs_graphviz` extension. For example, with **Mkdocs**, you add a
stanza to `mkdocs.yml`:

```yaml
markdown_extensions:
    - mkdocs_asy
```

## Complete Documentation

See the Repository Page : [mkdocs-asy on GitLab](https://gitlab.com/rodrigo.schwencke/mkdocs-asy)

