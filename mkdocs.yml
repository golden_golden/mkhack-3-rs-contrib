site_name: "MkHack3rs - Hacks & Integrations for Mkdocs / Material Theme"
site_author: eSkool, Rodrigo SCHWENCKE
site_url: https://eskool.gitlab.io/mkhack3rs/
site_description: >-
  Mon joli Site

# Copyright
copyright: Copyleft &#127279; 2021 - eSkool - Rodrigo SCHWENCKE - Licence CC BY-NC-SA 4.0

# Repository
repo_name: eskool/mkhack3rs
repo_url: https://gitlab.com/eskool/mkhack3rs

# Configuration
theme:
  name: material
  custom_dir: overrides
  logo: assets/images/eskool-nsi-blanc.svg
  favicon: assets/images/eskool-nsi-blanc.svg
  language: fr
  features:
    # empêche nouvelle requête:
    # ! ne pas activer 'navigate.instant':
    # - navigation.instant
    - navigation.tabs
    # - navigation.expand
    - navigation.top
    # Menus intégrés, ou pas:
    # - toc.integrate
    - header.autohide
    - navigation.tabs.sticky
    - navigation.sections
    # laisser 'navigation.indexes commenté, sinon les fichiers par défaut /index.md 
    # ne sont pas automatiquement affichés dans menu gauche
    # - navigation.indexes
  palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: red
        accent: red
        toggle:
            icon: material/weather-sunny
            name: Passer au mode jour

  favicon: assets/images/favicon.png
  icon:
    logo: logo
    admonition:
      note: octicons/tag-16
      abstract: octicons/checklist-16
      info: octicons/info-16
      tip: octicons/squirrel-16
      success: octicons/check-16
      question: octicons/question-16
      warning: octicons/alert-16
      failure: octicons/x-circle-16
      danger: octicons/zap-16
      bug: octicons/bug-16
      example: octicons/beaker-16
      quote: octicons/quote-16

# Extensions
markdown_extensions:
  - admonition
  - abbr
  - attr_list
  - def_list
  - footnotes
  - md_in_html
  - mkdocs_graphviz
      # light_theme: aquamarine
      # dark_theme: FFFFFF
      # color: FFFF00
      # bgcolor: 0000FF or none
      # graph_color: FF0000
      # graph_fontcolor: 00FF00
      # node_color: FF0000
      # node_fontcolor: FF0000
      # edge_color: FF0000
      # edge_fontcolor: FF0000
  - mkdocs_asy
  - meta
  - pymdownx.arithmatex:
      generic: true
      smart_dollar: false
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
      options:
        custom_icons:
          - overrides/.icons
  - pymdownx.highlight:
      linenums: None
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.magiclink:
      repo_url_shorthand: true
      user: rodrigo.schwencke
      repo: eskool/mkhack3rs
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.snippets
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_div_format
        # - name: graphviz
        #   class: graphviz
        #   format: !!python/name:graphviz.superfences_graphviz.format
        #   validator: !!python/name:graphviz.superfences_graphviz.validate
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - toc:
      permalink: ⚓︎
      toc_depth: 3

# Plugins
plugins:
  - search
  # - kroki
  - macros:
      site_url: https://eskool.gitlab.io/mkhack3rs/
  # - markmap
  # - mkdocs-jupyter:
  #     include_source: True
  # incompatible avec latex \xrightarrow etc...
  # - tooltips

  #- page-to-pdf # should be last
      # execute: True

  # PAS le contraire : index.md -> bases.md
  # car n'affiche pas les menus
  # - redirects:
  #     redirect_maps:
  #       web/ihm_python.md: python/ihm.md

# Customization
extra:
  social:
    - icon: fa/creative-commons
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-by
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-nc-eu
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-sa
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/paper-plane
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      link: mailto:contact.eskool@gmail.com
      name: Envoyer un email
    # - icon: fontawesome/brands/linux
    - icon: sk/matrix
      link: https://riot.im/app/#/room/#eskool:matrix.org
      name: "Rejoignez nous sur la Matrice:\n#eskool:matrix.org"
    - icon: fa/linux
      link: https://linuxfr.org/
      name: Linux is Cool!
    - icon: fa/gitlab
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      link: https://gitlab.com/eskool/mkhack3rs
      name: dépôt gitlab
    - icon: fa/university
      link: https://eskool.gitlab.io/accueil
      name: The eSkool Initiative
  raw_url: https://eskool.github.io/mkhack3rs/-/raw/main/docs/
  io_url: https://eskool.gitlab.io/mkhack3rs
  site_url: https://eskool.gitlab.io/mkhack3rs/

extra_css:
    - https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css
    # - https://unpkg.com/leaflet@1.7.1/dist/leaflet.css
    - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia.css
    # - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia-dark.css
    # - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia-maths.css
    # - assets/stylesheets/mermaid.css
    # - assets/stylesheets/mermaid-dark.css
    - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/mkdocs-mermaid.css
    # - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/mermaid-dark.css
    # - assets/stylesheets/hint.min.css
    # - assets/stylesheets/pyoditeur.css
    - https://pyscript.net/alpha/pyscript.css
    # - https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css


extra_javascript:
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  # - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  - https://cdn.jsdelivr.net/npm/mathjax@3.2.0/es5/tex-mml-chtml.js
  - https://cdnjs.cloudflare.com/ajax/libs/mermaid/8.14.0/mermaid.min.js

  - assets/javascripts/mathjax.js
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia-maths.js
  - assets/javascripts/mkdocs-mermaid.js
  # - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/mkdocs-mermaid.js
  - assets/javascripts/mkdocs-graphviz.js
  # - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/mkdocs-graphviz.js

  - https://tikzjax.com/v1/tikzjax.js
  # - src/js/pyodide.js

nav:
  - Home: index.md
  - Graphviz: 
    - Configuration: graphviz/index.md
    - Examples: graphviz/examples.md
  - Mermaid: 
    - Configuration: mermaid/index.md
    - Examples: mermaid/examples.md
  - PyScript: 
    - Configuration: pyscript/index.md
    - Examples: pyscript/examples.md
  - Multi-Columns:
    - Configuration: columns/index.md
    - Examples: columns/examples.md
  - Maths:
    - Typing Maths: maths/index.md
    - Maths Admonitions: 
      - Configuration: maths/admonitions/index.md
      - Examples: maths/admonitions/examples.md
    - Asy: 
      - Configuration: maths/asy/index.md
      - Examples: maths/asy/examples.md
    - Géogebra: maths/geogebra/index.md
  - Coding:
    - (TODO): coding/index.md
  - Tests:
      - Leaflet: sandbox/myleaflet.md
      - Graphviz: sandbox/mygraphviz.md
      - Plotly: sandbox/myplotly.md
      - D3.js: sandbox/myd3.md
      - Viz: sandbox/myviz.md